<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Shorty</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="{{asset('css/app.css')}}" rel="stylesheet">
    </head>
    <body>
        <div id="app">
            <div class="container" style="width: 1200px; margin: auto; margin-top: 40px">
                <form method="post" @submit.prevent="onSubmit">
                    <div class="control">
                        <input
                            class="input is-large"
                            type="url"
                            placeholder="Url here ..."
                            v-model="link"
                            @keydown="errors.clear('link')"
                        >
                        <span class="help is-danger" v-text="errors.get('link')"></span>
                    </div>
                    <button type="submit" class="button">Create new link</button>
                    </form>

                <hr>
                <table class="table" v-if="links.length > 0">
                    <thead>
                        <tr>
                            <th>Link</th>
                            <th>Code</th>
                            <th>Counter</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(link, index) in links" :key="index">
                            <td v-text="link.link"></td>
                            <td><a :href="link.code" v-text="link.code" v-on:click="link.counter += 1" target="_blank"></a></td>
                            <td v-text="link.counter"></td>
                        </tr>
                    </tbody>
                </table>

                <div v-else>There is no link yet</div>
            </div>

        </div>

    <script src="{{asset('js/app.js')}}"></script>
    </body>
</html>
