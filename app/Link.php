<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{

    protected $fillable = [
        'link',
        'code',
        'counter'
    ];


    public function getRouteKeyName()
    {
        return 'code';
    }
}
