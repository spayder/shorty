<?php

namespace App\Http\Controllers;

use App\Link;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class LinkController extends Controller
{
    public function index()
    {
        return response(['data' => Link::all()], Response::HTTP_OK);
    }

    public function store(Request $request)
    {
        $request->validate([
            'link' => 'required|url|unique:links,link'
        ]);

        $response = Link::create([
            'link' => $request->link,
            'code' => substr(md5(microtime()), rand(0,26), 6),
            'counter' => 0
        ]);

        return response(['data' => $response->jsonSerialize()], Response::HTTP_CREATED);
    }

    public function show(Link $link)
    {
        $link->increment('counter');
        $link->save();

        return redirect($link->link);
    }
}
