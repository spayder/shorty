
# Url shortener made with Laravel 6

## Introduction

Small app that allows to shorten a given url

## Installation and Setup

For installation please follow the following steps:

First run composer install

    composer install

Next create .env and after that config your database connection in it  
    
    cp .env.example .env
    
Next need to create a hash key

    php artisan key:generate
    
Run the migrations
    
    php artisan migrate
    
As this project uses Vue need to pull down all dependencies and run
     
    npm install
    npm run dev


Thats all!
